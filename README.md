[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

[![pipeline status](https://gitlab.com/jorgemustaine/openengrve/badges/11.0/pipeline.svg)](https://gitlab.com/openengrve/odoo_metafiles/commits/11.0)

[![coverage report](https://gitlab.com/jorgemustaine/openengrve/badges/11.0/coverage.svg)](https://gitlab.com/openengrve/odoo_metafiles/commits/11.0)
# My odoo test env

${REPO_DESCRIPTION}

[//]: # (addons)
This part will be replaced when running the oca-gen-addons-table script from OCA/maintainer-tools.
[//]: # (end addons)

----

OCA, or the [Odoo Community Association](http://odoo-community.org/), is a nonprofit organization whose
mission is to support the collaborative development of Odoo features and
promote its widespread use.
